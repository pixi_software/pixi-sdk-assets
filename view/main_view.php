<?php
/**if(!$_SESSION['logged_in']) {
header('location: '.site_url('login'));
}**/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title><?= (strlen($pixi_config_title) > 0) ? $pixi_config_title : null;  ?></title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- page specific styles -->
    <?= (isset($htmlHead)) ? $htmlHead : null; ?>

    <!-- jquery script -->
    <script src="<?=base_url()?>vendor/components/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>vendor/components/jqueryui/jquery-ui.min.js"></script>

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?=base_url()?>vendor/twitter/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>vendor/fortawesome/font-awesome/css/font-awesome.min.css" />

    <!-- SDK styles -->
    <link rel="stylesheet" href="<?=base_url()?>vendor/components/pixi/css/ace.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>vendor/components/pixi/css/pixiApps.css" />

</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default navbar-collapse">

    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">

            <a href="<?=base_url()?>" class="navbar-brand">
                <small class="pixi"><?= (strlen($pixi_config_title) > 0) ? $pixi_config_title : null;  ?></small>
            </a>

            <button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons">
                <span class="sr-only">Benutzermenü</span>
                <span class="ace-icon btn-app fa fa-wrench icon-only bigger-250 white"></span>
            </button>

            <button class="pull-right navbar-toggle collapsed btn-app" type="button" data-toggle="collapse" data-target=".sidebar">
                <span class="sr-only">Toggle sidebar</span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>

                <span class="icon-bar"></span>
            </button>

        </div>

        <div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
            <ul class="nav ace-nav">

                <li class="pixi-orange transparent">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <i class="ace-icon fa fa-user"></i>
                        <span>USERNAME</span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-hdd-o"></i>
                                Datenbank: DATABASE
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="<?= site_url('login?logout'); ?>">
                                <i class="ace-icon fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="main-container" id="main-container">

    <div id="sidebar" class="sidebar navbar-collapse collapse">

        <ul class="nav nav-list">
            <?php if(!empty($menu)) : ?>
                <?php foreach ($menu->menuItems as $menuitem) : ?>
                    <li <?php if ($menuitem->active()) { ?> class="active" <?php }; ?>>
                        <?php if (isset($menuitem->subMenu)) :?>
                        <a href="#" class="dropdown-toggle">
                            <?php else: ?>
                            <a href="<?=$menuitem->FullURL()?>">
                                <?php endif; ?>

                                <?php if ($menuitem->Icon != '') :?>
                                    <i class="menu-icon fa <?=$menuitem->Icon;?>"></i>
                                <?php endif;?>

                                <span class="menu-text"> <?=$menuitem->Text?> </span>
                                <?php if (!empty($menuitem->CountSign)) : ?>
                                    <span class="badge badge-primary "><?=$menuitem->CountSign?></span>
                                <?php endif; ?>
                                <?php if (isset($menuitem->subMenu)) : ?>
                                    <b class="arrow fa fa-angle-down"></b>
                                <?php endif;?>

                            </a>
                            <?php if (isset($menuitem->subMenu)) {?>
                                <ul class="submenu">
                                    <?php foreach ($menuitem->subMenu->menuItems  as $submenuitem) {?>
                                        <li <?php if ($submenuitem->active()) { ?> class="active" <?php }; ?>><a href="<?=$submenuitem->FullURL()?>">

                                                <i class="menu-icon fa fa-caret-right"></i>
                                                <span class="menu-text"><?=$submenuitem->Text;?></span>
                                                <?php if (!empty($submenuitem->CountSign)) {?>
                                                    <span class="badge badge-primary "><?=$submenuitem->CountSign?></span>
                                                <?php }; ?>

                                            </a></li>
                                    <?php };?>
                                </ul>
                            <?php };?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>

    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if(isset($body)) {
                            echo $body;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>

</div>

<script src="<?=base_url()?>vendor/twitter/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SDK scripts -->
<script src="<?=base_url()?>vendor/components/pixi/js/ace.min.js"></script>

<!-- page specific plugin scripts -->
<?php if(isset($htmlBottom)) {
    echo $htmlBottom;
} ?>

</body>
</html>